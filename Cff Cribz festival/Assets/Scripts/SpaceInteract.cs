﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpaceInteract : MonoBehaviour
{
    public List<GameObject> popups = new List<GameObject>();
    public string playerMessage;
    public enum InteractableType { Text, SceneTransition, Photo, Poster}
    public InteractableType interactID;
    public int sceneTransitionID;
    public Sprite posterSprite;
    public GameObject photoCamera;
    public GameObject photoLight;

    void Awake()
    {
        InteractPopOff();
    }

    public void InteractPopUp()
    {
        foreach (GameObject pop in popups)
        {
            if (pop.gameObject.activeSelf == false)
                pop.gameObject.SetActive(true);
        }   
    }

    public void InteractPopOff()
    {
        foreach (GameObject pop in popups)
        {
            if (pop.gameObject.activeSelf == true)
                pop.gameObject.SetActive(false);
        }
    }
}
