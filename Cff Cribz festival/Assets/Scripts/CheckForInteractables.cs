﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class CheckForInteractables : MonoBehaviour
{
    private Collider interactTrig;
    [SerializeField]
    private bool isInField = false;
    [SerializeField]
    private GameObject playerTextPopup;
    private GameObject currentInteractable;
    private bool isShowingTextPopup;
    private bool isShowingPoster;
    private bool canTakePhoto = true;
    [SerializeField]
    private GameObject cameraObj;
    private GameObject photoQuad;

    private void Awake()
    {
        foreach(Transform child in transform)
        {
            if (child.gameObject.GetComponent<MeshRenderer>() != null)
            {
                playerTextPopup = child.gameObject;
                playerTextPopup.gameObject.SetActive(false);
            }
        }
        StartCoroutine("getCamera");
    }

    private void Update()
    {
        if (Input.GetButtonUp("Jump") && isInField)
        {
            ActivatePlayerText(currentInteractable.GetComponent<SpaceInteract>().playerMessage);
            if (currentInteractable.GetComponent<SpaceInteract>().interactID == SpaceInteract.InteractableType.SceneTransition)
                StartCoroutine("sceneTransitionTimer", currentInteractable.GetComponent<SpaceInteract>().sceneTransitionID);
            else if (currentInteractable.GetComponent<SpaceInteract>().interactID == SpaceInteract.InteractableType.Poster)
                ActivatePosterDisplay(currentInteractable.GetComponent<SpaceInteract>().posterSprite);
            else if (currentInteractable.GetComponent<SpaceInteract>().interactID == SpaceInteract.InteractableType.Photo && canTakePhoto)
                StartCoroutine("displayPhotoTimer", currentInteractable.GetComponent<SpaceInteract>().photoLight);
        }
    }

    //entering field
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            ActivatePopup(other.gameObject);
            currentInteractable = other.gameObject;
            isInField = true;
        }
    }


    //leaving field
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            isInField = false;
            if (isShowingTextPopup)
                DeactivatePopup(other.gameObject);
            if (isShowingPoster)
                DeactivatePosterDisplay();
        }
    }

    private void ActivatePopup(GameObject target)
    {
        isShowingTextPopup = true;
        target.GetComponent<SpaceInteract>().InteractPopUp();
    }
    private void DeactivatePopup(GameObject target)
    {
        isShowingTextPopup = false;
        target.GetComponent<SpaceInteract>().InteractPopOff();
    }

    void ActivatePlayerText(string textInput)
    {
        playerTextPopup.SetActive(true);
        playerTextPopup.GetComponent<TextMeshPro>().SetText(textInput);
        StartCoroutine("textDisplayTime");
    }
    void DeactivatePlayerText()
    {
        playerTextPopup.SetActive(false);
        playerTextPopup.GetComponent<TextMeshPro>().SetText("/");
    }

    private void ActivatePosterDisplay(Sprite posterSprite)
    {
        foreach(Transform child in cameraObj.transform)
        {
            if (child.gameObject.GetComponent<SpriteRenderer>()!= null && child.gameObject.activeSelf == false)
            {
                child.gameObject.SetActive(true);
                child.gameObject.GetComponent<SpriteRenderer>().sprite = posterSprite;
                isShowingPoster = true;
            }
        }
    }
    private void DeactivatePosterDisplay()
    {
            foreach (Transform child in cameraObj.transform)
            {
                if (child.gameObject.GetComponent<SpriteRenderer>() != null && child.gameObject.activeSelf == true)
                {
                    child.gameObject.SetActive(false);
                    isShowingPoster = false;
                }
            }
    }

    public IEnumerator textDisplayTime()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        DeactivatePlayerText();
        yield break;
    }

    public IEnumerator sceneTransitionTimer(int ID)
    {
        yield return new WaitForSecondsRealtime(1f);
        SceneManager.LoadScene(ID);
        yield break;
    }

    public IEnumerator getCamera()
    {
        yield return null;
        if (this.gameObject.scene.buildIndex == -1)
        {
            cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
            foreach (Transform child in cameraObj.transform)
            {
                if (child.gameObject.GetComponent<SpriteRenderer>() == null && child.gameObject.activeSelf == false)
                {
                    photoQuad = child.gameObject;
                }
            }
        }
        yield break;
    }

    public IEnumerator displayPhotoTimer(GameObject photoLight)
    {
        photoBoothActiveSwitch(photoLight);
        yield return new WaitForSecondsRealtime(3f);
        photoBoothActiveSwitch(photoLight);
        yield break;
    }

    private void photoBoothActiveSwitch(GameObject light)
    {
        photoQuad.SetActive(!photoQuad.activeSelf);
        canTakePhoto = !canTakePhoto;
        light.SetActive(!light.activeSelf);
    }
}

