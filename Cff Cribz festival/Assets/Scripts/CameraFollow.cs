﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject player;
    public float cameraDistance;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        gameObject.transform.position= (new Vector3(player.transform.position.x, this.gameObject.transform.position.y, player.transform.position.z - cameraDistance));
    }
}
