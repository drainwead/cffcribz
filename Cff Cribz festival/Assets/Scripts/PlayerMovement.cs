﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float AccelerationTime = 1;
    public float DecelerationTime = 1;
    public float MaxSpeed = 1;
    private Vector3 accelTime = new Vector3(0, 0, 0);

    
    private Vector3 movement;
    private Vector3 lookDir = new Vector3(0,0,0);
    private bool canMove = true;

    public LayerMask obstacleLayer;

    private GameObject visualsObj;
    private void Awake()
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.GetComponent<SphereCollider>() == null)
            {
                visualsObj = child.gameObject;
            }
        }
    }

    void FixedUpdate()
    {
        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f || (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f))
        {
            Vector2 currentInput = new Vector2(Input.GetAxis("Horizontal"), (Input.GetAxis("Vertical")));
            HandleMovementInput(currentInput.normalized);
            if (canMoveToLocation(movement.normalized))
            {
                transform.Translate(movement, Space.World);
                SetLookDirection(currentInput.normalized);
            }
            else
            {
                accelTime.x -= Time.deltaTime / (DecelerationTime/3);
                if (accelTime.x < 0)
                    accelTime.x = 0;

                accelTime.z -= Time.deltaTime / (DecelerationTime/3);
                if (accelTime.z < 0)
                    accelTime.z = 0;
            }
            
        }
        else
        {
            Deccelerate();
        }
    }

    void SetLookDirection(Vector3 direction)
    {
        direction = new Vector3(direction.x, direction.z, direction.y);
        lookDir = (direction * 10);
        visualsObj.transform.LookAt(transform.position + lookDir, Vector3.up);

    }

    public void HandleMovementInput(Vector2 input)
    {

        //horizontal
        if (input.x != 0)
        {
            accelTime.x += Time.deltaTime / AccelerationTime;
            movement.x = Mathf.Lerp(0, Mathf.Sign(input.x) * MaxSpeed * Time.deltaTime, accelTime.x);
            if (accelTime.x > 1)
                accelTime.x = 1;
        }

        if (input.x == 0 && movement.x != 0)
        {
            accelTime.x -= Time.deltaTime / (DecelerationTime/3);
            movement.x = Mathf.Lerp(0, Mathf.Sign(movement.x) * MaxSpeed * Time.deltaTime, accelTime.x);
            if (accelTime.x < 0)
                accelTime.x = 0;
        }

        //vertical
        if (input.y != 0)
        {
            accelTime.z += Time.deltaTime / AccelerationTime;
            movement.z = Mathf.Lerp(0, Mathf.Sign(input.y) * MaxSpeed * Time.deltaTime, accelTime.z);
            if (accelTime.z > 1)
                accelTime.z = 1;
        }
        if (input.y == 0 && movement.z != 0)
        {
            accelTime.z -= Time.deltaTime / (DecelerationTime/3);
            movement.z = Mathf.Lerp(0, Mathf.Sign(movement.z) * MaxSpeed * Time.deltaTime, accelTime.z);
            if (accelTime.z < 0)
                accelTime.z = 0;
        }
    }

    private bool canMoveToLocation(Vector3 potentialLoc)
    {
        Vector3 moveToPoint = new Vector3((transform.position.x + potentialLoc.x * MaxSpeed * Time.deltaTime * accelTime.magnitude), 
            transform.position.y, (transform.position.z + potentialLoc.z * MaxSpeed * Time.deltaTime * accelTime.magnitude));

        Vector3 castdir = (moveToPoint - transform.position).normalized;
        if (!Physics.BoxCast(transform.position, new Vector3(0.1f,0.5f,0.1f),castdir,Quaternion.identity,
            Vector3.Distance(transform.position,moveToPoint),obstacleLayer))
            return true;
        else
            return false;
    }

    void Deccelerate()
    {
        //deccel + stop
        accelTime.x -= Time.deltaTime / DecelerationTime;
        movement.x = Mathf.Lerp(0, Mathf.Sign(movement.x) * MaxSpeed * Time.deltaTime, accelTime.x);
        if (accelTime.x < 0)
            accelTime.x = 0;

        accelTime.z -= Time.deltaTime / DecelerationTime;
        movement.z = Mathf.Lerp(0, Mathf.Sign(movement.z) * MaxSpeed * Time.deltaTime, accelTime.z);
        if (accelTime.z < 0)
            accelTime.z = 0;
    }
}
