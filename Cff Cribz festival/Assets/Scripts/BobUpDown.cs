﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobUpDown : MonoBehaviour
{
    public float bobDistance = 0.001f;
    public float bobSpeed = 1f;

    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + bobDistance*(Mathf.Sin(Time.time*bobSpeed)), transform.position.z);
    }
}
