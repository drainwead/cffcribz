﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public List<GameObject> nonDestructables = new List<GameObject>();
    void Awake()
    {
        if (OtherManagersPresent() == false)
        {
            DontDestroyOnLoad(this.gameObject);
            foreach (GameObject go in nonDestructables)
            {
                //Instantiate(go);
                DontDestroyOnLoad(Instantiate(go));
            }
        }
        else
            Destroy(this.gameObject);
    }


    bool OtherManagersPresent()
    {
        if (GameObject.FindGameObjectsWithTag("GameController") != null)
        {
            foreach (GameObject goo in GameObject.FindGameObjectsWithTag("GameController"))
            {
                if (goo.scene.buildIndex == -1 && goo != this.gameObject)
                    return true;
                else
                    return false;
            }
            return true;
        }
        else
            return true;
    }
}
